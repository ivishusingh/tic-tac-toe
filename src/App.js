import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { unwatchFile } from "fs";
import { ifError } from "assert";

class App extends Component {
  state = {
    value: "",
    player1: "x",
    player2: "0",
    default: "",
    default1: "",
    default2: "",
    default3: "",
    default4: "",
    default5: "",
    default6: "",
    default7: "",
    default8: "",
    default9: "",
    winner: null
  };
  reset = () => {
    this.setState({
      value: "",
      default: "",
      default1: "",
      default2: "",
      default3: "",
      default4: "",
      default5: "",
      default6: "",
      default7: "",
      default8: "",
      default9: ""
    });
  };
  game1 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default1: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default1: this.state.player2
      });
    }
  };
  game2 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default2: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default2: this.state.player2
      });
    }
  };

  game3 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default3: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default3: this.state.player2
      });
    }
  };

  game4 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default4: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default4: this.state.player2
      });
    }
  };
  game5 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default5: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default5: this.state.player2
      });
    }
  };
  game6 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default6: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default6: this.state.player2
      });
    }
  };
  game7 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default7: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default7: this.state.player2
      });
    }
  };
  game8 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default8: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default8: this.state.player2
      });
    }
  };
  game9 = () => {
    if (
      this.state.default === "" ||
      this.state.default === this.state.player2
    ) {
      this.setState({
        default: this.state.player1,
        default9: this.state.player1
      });
    } else {
      this.setState({
        default: this.state.player2,
        default9: this.state.player2
      });
    }
  };
  checkForWinner = () => {
    if (
      (this.state.default1 === this.state.default2 &&
        this.state.default2 === this.state.default3) ||
      (this.state.default4 === this.state.default5 &&
        this.state.default5 === this.state.default6)
    ) {
      this.setState({ winner: "win the game" });
    }
  };

  render() {
    return (
      <div className="App">
        <h1>TIC TAC TOE</h1>
        <h2>{this.state.winner}</h2>
        <table>
          <tr>
            <td onClick={this.game1}>{this.state.default1}</td>
            <td onClick={this.game2} className="vertical">
              {this.state.default2}
            </td>
            <td onClick={this.game3}>{this.state.default3}</td>
          </tr>
          <tr>
            <td onClick={this.game4} className="horizontal">
              {this.state.default4}
            </td>
            <td onClick={this.game5} className="vertical horizontal">
              {this.state.default5}
            </td>
            <td onClick={this.game6} className="horizontal">
              {this.state.default6}
            </td>
          </tr>
          <tr>
            <td onClick={this.game7}>{this.state.default7}</td>
            <td onClick={this.game8} className="vertical">
              {this.state.default8}
            </td>
            <td onClick={this.game9}> {this.state.default9}</td>
          </tr>
        </table>
        <br />
        <button
          onClick={this.reset}
          style={{
            backgroundColor: "red",
            color: "white",
            padding: "6px",
            border: "none",
            width: "90px"
          }}
        >
          Reset
        </button>
      </div>
    );
  }
}

export default App;
